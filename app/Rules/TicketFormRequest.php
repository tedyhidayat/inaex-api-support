<?php

namespace App\Rules;

use CHHW\FormRequest\FormRequest;

class TicketFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'depid' => 'required',
            'operatorid' => 'required',
            'clientid' => 'required',
            'priorityid' => 'required',
            'private' => 'required',
            'toptionid' => 'required',
            'status' => 'required',
            'attachments' => 'required',
            'mergeid' => 'required',
            'mergeopid' => 'required',
            'mergetime' => 'required',
            'initiated' => 'required',
            'ended' => 'required',
            'updated' => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'depid.required' => 'depid tidak boleh kosong',
            'operatorid.required' => 'operatorid tidak boleh kosong',
            'clientid.required' => 'clientid tidak boleh kosong',
            'priorityid.required' => 'priorityid tidak boleh kosong',
            'private.required' => 'private tidak boleh kosong',
            'toptionid.required' => 'toptionid tidak boleh kosong',
            'status.required' => 'status tidak boleh kosong',
            'attachments.required' => 'attachments tidak boleh kosong',
            'mergeid.required' => 'mergeid tidak boleh kosong',
            'mergeopid.required' => 'mergeopid tidak boleh kosong',
            'mergetime.required' => 'mergetime tidak boleh kosong',
            'initiated.required' => 'initiated tidak boleh kosong',
            'ended.required' => 'ended tidak boleh kosong',
            'updated.required' => 'updated tidak boleh kosong',
        ];
    }
}

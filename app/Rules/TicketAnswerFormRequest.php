<?php

namespace App\Rules;

use CHHW\FormRequest\FormRequest;

class TicketAnswerFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticketid' => 'required',
            'clientid' => 'required',
            'private' => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ticketid.required' => 'ticketid tidak boleh kosong',
            'clientid.required' => 'clientid tidak boleh kosong',
            'private.required' => 'private tidak boleh kosong',
        ];
    }
}

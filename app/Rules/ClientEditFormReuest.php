<?php

namespace App\Rules;

use CHHW\FormRequest\FormRequest;

class ClientEditFormReuest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'email' => 'required',
            'name' => 'required'
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'id tidak boleh kosong',
            'email.required' => 'email tidak boleh kosong',
            'name.required' => 'name tidak boleh kosong',
        ];
    }
}

<?php

namespace App\Constants;

use App\Constants\ErrorCode as EC;
use App\Constants\ErrorMessage as EM;
use App\Exceptions\CustomException;


class Helper{

    static function responseData($data = false, $paginate = null){
        if($paginate == null){
            $response = ["meta" => [
                'status' => "OK",
                'code' => 200,
                'message' => EM::NONE,
                'error' =>  EC::NOTHING],
                "data" => $data ];

            if ($data === false) unset($response['data']);
        }else{
            $response = ["meta" => ['status' => "OK",
                'code' => 200,'message' => EM::NONE, 'error' => EC::NOTHING, 'page' => $paginate ],
                "data" => $data ];
        }

        return response()->json($response, 200);
    }

    static function responseIUData($status){

        $response = ["meta" => [
            'status' => "OK",
            'code' => 200,
            'message' => EM::NONE,
            'error' =>  EC::NOTHING,
            'details' => $status],
            "data" => [] ];

        return response()->json($response, 200);
    }

    static function responseCatchData($code, $message, $error){

        $response = ["meta" => [
            'status' => "OK",
            'code' => $code,
            'message' => $message,
            'error' =>  $error],
            "data" => [] ];

        return response()->json($response, 200);
    }

    static function getContentType($fileName){
        $path_parts = pathinfo($fileName);
        $ext = strtolower($path_parts["extension"]);
        $mime = [
            'doc' => 'application/msword',
            'dot' => 'application/msword',
            'sfdt' => 'application/json',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'docm' => 'application/vnd.ms-word.document.macroEnabled.12',
            'dotm' => 'application/vnd.ms-word.template.macroEnabled.12',
            'xls' => 'application/vnd.ms-excel',
            'xlt' => 'application/vnd.ms-excel',
            'xla' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'xlsm' => 'application/vnd.ms-excel.sheet.macroEnabled.12',
            'xltm' => 'application/vnd.ms-excel.template.macroEnabled.12',
            'xlam' => 'application/vnd.ms-excel.addin.macroEnabled.12',
            'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'ppt' => 'application/vnd.ms-powerpoint',
            'pot' => 'application/vnd.ms-powerpoint',
            'pps' => 'application/vnd.ms-powerpoint',
            'ppa' => 'application/vnd.ms-powerpoint',
            'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
            'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'ppam' => 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
            'pptm' => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
            'potm' => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
            'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
            'pdf' => 'application/pdf',
            'png' => 'image/png',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpg'
        ];

        return isset($mime[$ext]) ? $mime[$ext] : 'application/octet-stream';
    }

    static function password_creator($length = 8) {
        return substr(md5(rand().rand()), 0, $length);
    }

    static function generateRandStr($length) {
        $randstr = "";
        for($i=0; $i<$length; $i++){
            $randnum = mt_rand(0,61);
            if($randnum < 10){
                $randstr .= chr($randnum+48);
            }else if($randnum < 36){
                $randstr .= chr($randnum+55);
            }else{
                $randstr .= chr($randnum+61);
            }
        }
        return $randstr;
    }

    static function generateRandID() {
        return md5(Helper::generateRandStr(16));
    }
}

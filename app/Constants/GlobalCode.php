<?php

namespace App\Constants;

class GlobalCode
{
    const DEFAULT_LIMIT = 10;
    const GENERAL_APP   = 'inaexport_';
    const ROOT = 'files/support/';
    const DB_PASS_HASH = 'something_strong_goes_in_here';
    const FILTER_SANITIZE_EMAIL = 517;
}

<?php

namespace App\Models\v1;

use App\Constants\ErrorCode as EC;
use App\Constants\ErrorMessage as EM;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use App\Constants\GlobalCode as GC;
use App\Constants\Helper;
use DB;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use DateTime;

class Ticket extends Model{

    protected $table = GC::GENERAL_APP.'support_tickets';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;

    public function department(){

        return $this->hasOne(Department::class, 'id','depid')->select("title","id");
    }

    static function retrieve($query = NULL){

        $search = $query->search;
        $order_by = $query->order ? $query->order : 'id';
        $sort = $query->sort ? $query->sort : 'DESC';
        $clientid = $query->clientid;

        try {
            $data = Ticket::select('*');
            $data->with('department');
            if($clientid)
                $data->where('clientid', $clientid);

            if($search) {
                    $data->where('depid', 'like', '%' . $search . '%')
                        ->orWhere('operatorid', 'like', '%' . $search . '%')
                        ->orWhere('clientid', 'like', '%' . $search . '%')
                        ->orWhere(DB::raw('lower(name)'), 'like', '%' . strtolower($search) . '%')
                        ->orWhere(DB::raw('lower(email)'), 'like', '%' . strtolower($search) . '%')
                        ->orWhere('priorityid', 'like', '%' . $search . '%')
                        ->orWhere(DB::raw('lower(subject)'), 'like', '%' . $search . '%')
                        ->orWhere('content', 'like', '%' . $search . '%')
                        ->orWhere('ip', 'like', '%' . $search . '%')
                        ->orWhere(DB::raw('lower(referrer)'), 'like', '%' . strtolower($search) . '%')
                        ->orWhere(DB::raw('lower(notes)'), 'like', '%' . strtolower($search) . '%')
                        ->orWhere('private', 'like', '%' . $search . '%')
                        ->orWhere('status', 'like', '%' . $search . '%')
                        ->orWhere('attachments', 'like', '%' . $search . '%')
                        ->orWhere('mergeid', 'like', '%' . $search . '%')
                        ->orWhere('mergeopid', 'like', '%' . $search . '%')
                        ->orWhere('mergetime', 'like', '%' . $search . '%')
                        ->orWhere('initiated', 'like', '%' . $search . '%')
                        ->orWhere('ended', 'like', '%' . $search . '%')
                        ->orWhere('updated', 'like', '%' . $search . '%')
                        ->orWhere('reminder', 'like', '%' . $search . '%');
            }

            $datas = $data->orderBy($order_by, $sort)->paginate(GC::DEFAULT_LIMIT);
            $paginate = [
                'total' => $datas->total(),
                'current_page' => $datas->currentPage(),
                'from' => $datas->currentPage(),
                'per_page' => $datas->perPage(),
            ];
            $result = new \stdClass;
            $result->items = $datas->items();
            $result->paginate = $paginate;
            $hasil = Helper::responseData($result->items, $result->paginate);
            return $hasil;
        }catch (\Exception $m){
            return $m;
        }
    }


    static function retrieveById($id){

        try{
            $data['ticket'] = Ticket::from(GC::GENERAL_APP.'support_tickets as st')
                                ->select('st.id','st.name','st.email','st.subject', 'st.content', 'dep.title as department', 'status.title as status',
                                          'pr.title as priority', 'to.title', 'st.duedate','st.initiated')
                                ->join(GC::GENERAL_APP.'support_departments as dep', 'st.depid', 'dep.id')
                                ->join(GC::GENERAL_APP.'support_status as status', 'st.status', 'status.id')
                                ->join(GC::GENERAL_APP.'ticketpriority as pr', 'st.priorityid', 'pr.id')
                                ->join(GC::GENERAL_APP.'ticketoptions as to', 'st.toptionid', 'to.id')
                                ->where('st.id', $id)
                                ->first();

            $data['answer'] = TicketAnswer::where('ticketid', $id)->get();

            $hasil = Helper::responseData($data);
            return $hasil;
        }catch (\Exception $m){
            return $m;
        }
    }

    static function updateTicketAttach($id, $attach){

        try {
            $ticket = Ticket::where('id', $id)->update(['attachments' => $attach]);
            return Helper::responseIUData($ticket);
        }catch (\Exception $m){
            return $m;
        }
    }

    static function updateTicketStatus($id, $status){

        try {
            $ticket = Ticket::where('id', $id)->update(['status' => $status]);
            return Helper::responseIUData($ticket);
        }catch (\Exception $m){
            return $m;
        }
    }

    static function insert($query = NULL){

        try {

            $filesystem = Flysystem::connection('sftp');
            $data["depid"] = $query->depid;
            $data["operatorid"] = $query->operatorid;
            $data["clientid"] = $query->clientid;
            $data["name"] = $query->name;
            $data["email"] = $query->email;
            $data["priorityid"] = $query->priorityid;
            $data["toptionid"] = $query->toptionid;
            $data["subject"] = $query->subject;
            $data["content"] = $query->content;
            $data["ip"] = $query->ip;
            $data["referrer"] = $query->referrer;
            $data["notes"] = $query->notes;
            $data["private"] = $query->private;
            $data["status"] = $query->status;
            $data["attachments"] = $query->attachments;
            $data["mergeid"] = $query->mergeid;
            $data["mergeopid"] = $query->mergeopid;
            $data["mergetime"] = $query->mergetime;
            $data["initiated"] = $query->initiated;
            $data["ended"] = $query->ended;
            $data["updated"] = $query->updated;
            $data["duedate"] = $query->duedate;
            $data["reminder"] = $query->reminder;
            $file = $query->file('file');

            $ticket = Ticket::Create($data);

            if($query->has('file')){
                $destination = GC::ROOT . $ticket->id . "/";
                $filename = 'u_' . str_replace('.', '_', microtime(true)) . '_' . $file->getClientOriginalName();

                if (!$filesystem->has($destination))
                    $filesystem->createDir($destination);

                $upload = $filesystem->put($destination.$filename, file_get_contents($file));
                if($upload){

                    $ins  = TicketAnswer::Create([
                        "ticketid"    => $ticket->id,
                        "clientid"    => $data["clientid"],
                        "operatorid"  => 0,
                        "content"     => $filename,
                        "private"     => $data["private"],
                        "file"        => 1,
                        "lastedit"    => date('Y-m-d H:i:s'),
                        "sent"        => date('Y-m-d H:i:s')
                    ]);

                    return Helper::responseIUData($ins);
                }else
                    return Helper::responseCatchData(415, "Gagal Upload", $upload);
            }else{
                return Helper::responseCatchData(200,EM::NONE, EC::NOTHING);
            }

        }catch (\Exception $m){
            return $m;
        }
    }

    static function convertMiliToDate($timestamp){

      try {

          date_default_timezone_set('Asia/Bangkok');
          if (is_numeric($timestamp)) {
              $unixtime = $timestamp;
          } else {
              $unixtime = strtotime($timestamp);
          }

          $date = 'd.m.Y';
          $time = 'H:i';
          $ret = !empty($timestamp) ? date(($date && $time ? $date.' ' : $date).$time, $unixtime) : "";

          return Helper::responseIUData($ret);

      }catch (Exception $m){
          return $m;
      }
    }
}

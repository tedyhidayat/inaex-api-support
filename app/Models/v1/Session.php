<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use App\Constants\GlobalCode as GC;
use App\Constants\Helper;
use DB;

class Session extends Model{

    protected $table = GC::GENERAL_APP.'sessions';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;

    static function closeChat($email){

        try {
            $get = Session::where('email', $email)->max('id');
            $upd = Session::where('id', $get)->update(['status' => 0, 'ended' => time()]);

            return Helper::responseIUData($upd);

        }catch (Exception $m){
            return $m;
        }

    }

}

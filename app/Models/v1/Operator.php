<?php

namespace App\Models\v1;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use App\Constants\GlobalCode as GC;
use App\Constants\Helper;
use DB;
use Prophecy\Exception\Exception;
use Illuminate\Support\Facades\Crypt;

class Operator extends Model{

    protected $table = GC::GENERAL_APP.'user';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;

    static function insert($request = NULL){

        try {

            $cek = Operator::checkUser($request->email, $request->username);

            if($cek == 0) {
                $data["chat_dep"] = 0;
                $data["support_dep"] = 0;
                $data["password"] = hash_hmac('sha256', $request->password, GC::DB_PASS_HASH);
                $data["password_real"] = $request->password;
                $data["username"] = trim($request->username);
                $data["name"] = trim($request->name);
                $data["email"] = filter_var($request->email, GC::FILTER_SANITIZE_EMAIL);
                $data["responses"] = 1;
                $data["picture"] = '/standard.jpg';
                $data["files"] = 1;
                $data["operatorchat"] = 1;
                $data["operatorchatpublic"] = 1;
                $data["operatorlist"] = 1;
                $data["transferc"] = 1;
                $data["chat_latency"] = 3000;
                $data["push_notifications"] = 1;
                $data["useronlinelist"] = 1;
                $data["sound"] = 1;
                $data["ringing"] = 3;
                $data["alwaysnot"] = 1;
                $data["alwaysonline"] = 1;
                $data["emailnot"] = 0;
                $data["navsidebar"] = 1;
                $data["themecolour"] = 'green';
                $data["permissions"] = 'leads,leads_all,off_all,support,faq,blog,ochat,ochat_all,statistic,statistic_all,files,proactive,usrmanage,client,responses,departments,settings,maintenance,logs,answers,widget,groupchat';
                $data["access"] = 1;
                $data["forgot"] = 0;
                $data["hits"] = 0;
                $data["time"] = date('Y-m-d H:i:s');
                $ins = Operator::Create($data);
                return Helper::responseIUData($ins->id);

            }else{
                return Helper::responseCatchData(433, 'Email atau Username sudah terdaftar', "0");
            }

        }catch (\Exception $m){
            return $m;
        }
    }

    static function edit($request){

        try{

            $edit = Operator::where('id',$request->id);
            if($edit->count() > 0) {
                $dataedit = $edit->first();
                $data["password"] = !empty($request->password) ? hash_hmac('sha256', $request->password, GC::DB_PASS_HASH) : $dataedit->password;
                $data["username"] = trim($request->username);
                $data["name"]     = trim($request->name);
                $data["email"]    = filter_var($request->email, GC::FILTER_SANITIZE_EMAIL);
                $data["password_real"] = !empty($request->password) ? $request->password : $dataedit->password_real;

                if($request->email != $dataedit->email){
                    $cek_email = self::checkSpesific('email', $request->email);
                    if($cek_email != 0)
                        return Helper::responseCatchData(413, 'Support Helpdesk : Email telah digunakan oleh user lain', '0');
                }

                if($request->username != $dataedit->username){
                    $cek_username = self::checkSpesific('username', $request->username);
                    if($cek_username != 0)
                        return Helper::responseCatchData(413, 'Support Helpdesk : Username telah digunakan oleh user lain', '0');
                }

                $upd = Operator::where('id', $request->id)->update($data);
                return Helper::responseIUData($upd);

            }else{
                return Helper::responseCatchData(413, 'Support Helpdesk : Akun Belum Terdaftar', '0');
            }

        }catch (Exception $m){
            return $m;
        }
    }

    static function sessionLogin($id_helpdesk){

        try{
            $op = Operator::select('username', 'password', 'password_real', 'idhash', 'session')->where('id', $id_helpdesk);
            if($op->count() > 0) {
                $dataop = $op->first();
                $data["username"] = $dataop->username;
                $data["password"] = $dataop->password;
                $data["password_real"] = $dataop->password_real;
                $data["idhash"] = $dataop->id_hash;
                $data["session"] = $dataop->session;
                return Helper::responseData($data);
            }else{
                return Helper::responseCatchData(433, 'Akun Tidak Terdaftar', '0');
            }
        }catch (Exception $m){
            return $m;
        }
    }

    static function logout($id){

        try {
            $login = Operator::find($id);
            if ($login) {
                $login->update(['idhash' => null,
                    'lastactivity' => time(),
                    'session' => null]);
                return Helper::responseIUData($login->id);
            } else
                return Helper::responseCatchData(415,"Akun tidak ditemukan", 0);
        }catch (\Exception $m){
            return $m;
        }

    }

    static function checkUser($email, $username){
        try{
            $op = Operator::where('email', $email)
                ->where('username', str_replace(' ', '', $username))
                ->count();
            return $op;
        }catch (Exception $m){
            return $m;
        }

    }

    static function checkSpesific($column, $spesific){

        try {
            $op = Operator::where($column, str_replace(' ', '', $spesific))->count();
            return $op;
        }catch (Exception $m){
            return $m;
        }
    }

    static function destroy($id){
        try{
            $del = Operator::where('id',$id)->delete();
            return Helper::responseIUData($del);
        }catch (Exception $m){
            return $m;
        }

    }

}

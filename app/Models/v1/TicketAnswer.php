<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use App\Constants\GlobalCode as GC;
use App\Constants\Helper;
use DB;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Storage;

class TicketAnswer extends Model{

    protected $table = GC::GENERAL_APP.'ticket_answers';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;

    static function insert($query = NULL){

        try {

            $data["ticketid"] = $query->ticketid;
            $data["clientid"] = $query->clientid;
            $data["private"] = $query->private;
            $destination = GC::ROOT . $data["ticketid"] . "/";
            $filesystem = Flysystem::connection('sftp');

            if ($query->has('content')) {
                $file =  $query->file('content');;
                $filename = 'u_' . str_replace('.', '_', microtime(true)) . '_' . $file->getClientOriginalName();

                if (!$filesystem->has($destination))
                    $filesystem->createDir($destination);

                $upload = $filesystem->put($destination.$filename, file_get_contents($file));
                if($upload){

                  $ins  = TicketAnswer::Create([
                        "ticketid"    => $data["ticketid"],
                        "clientid"    => $data["clientid"],
                        "operatorid"  => 0,
                        "content"     => $filename,
                        "private"     => $data["private"],
                        "file"        => 1,
                        "lastedit"    => date('Y-m-d H:i:s'),
                        "sent"        => date('Y-m-d H:i:s')
                    ]);

                    return Helper::responseIUData($ins);
                }else
                    return Helper::responseCatchData(415, "Gagal Upload", $upload);
            }else{

                $ins  = TicketAnswer::Create([
                    "ticketid"    => $data["ticketid"],
                    "clientid"    => $data["clientid"],
                    "operatorid"  => 0,
                    "content"     => $query->contents,
                    "private"     => $data["private"],
                    "file"        => 0,
                    "lastedit"    => date('Y-m-d H:i:s'),
                    "sent"        => date('Y-m-d H:i:s')
                ]);

                return Helper::responseIUData($ins);
            }
        }catch (\Exception $m){
            return $m;
        }
    }
}

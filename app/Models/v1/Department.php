<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use App\Constants\GlobalCode as GC;
use App\Constants\Helper;
use DB;

class Department extends Model{

    protected $table = GC::GENERAL_APP.'support_departments';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;

    static function retrieve(){
        try {
            $data = Department::all();
            $hasil = Helper::responseData($data);
            return $hasil;
        }catch (\Exception $m){
            return $m;
        }
    }

}

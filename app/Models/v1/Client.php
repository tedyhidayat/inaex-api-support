<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use App\Constants\GlobalCode as GC;
use App\Constants\Helper;
use DB;
use Prophecy\Exception\Exception;

class Client extends Model{

    protected $table = GC::GENERAL_APP.'clients';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;

    static function insert($request = NULL){

        try {

            $data["chat_dep"] = 0;
            $data["support_dep"] = 0;
            $data["faq_cat"] = 0;
            $data["email"] = $request->email;
            $data["name"] = $request->name;
            $data["picture"] = '/standard.jpg';
            $data["password"] = hash_hmac('sha256', $request->password, GC::DB_PASS_HASH);
            $data["time"] = date('Y-m-d H:i:s');
            $data["supportrequests"] = 0;
            $data["canupload"] = 1;
            $data["hits"] = 0;
            $data["access"] = 1;
            $data["frontendadmin"] = 0;
            $data["custom_price"] = 0;
            $data["credits"] = 0;
            $data["paid_until"] = '1980-05-06';
            $data["support_until"] = '1980-05-06';
            $data["forgot"] = 0;
            $ins = Client::Create($data);
            return Helper::responseIUData($ins->id);

        }catch (\Exception $m){
            return $m;
        }
    }

    static function edit($request = NULL){

        try{
            $pass_awal = Client::where('id', $request->id)->first()->password;

            $data["email"] = $request->email;
            $data["name"]  = $request->name;
            $data["password"]  = !empty($request->password) ? hash_hmac('sha256', $request->password, GC::DB_PASS_HASH) : $pass_awal;

            $upd = Client::where('id',$request->id)->update($data);
            return Helper::responseIUData($upd);

        }catch (\Exception $m){
            return $m;
        }
    }

    static function login($id){

        try {
            $login = Client::find($id);
            if ($login) {
                $login->update(['idhash' => Helper::generateRandID(),
                                'logins' => $login->first()->logins + 1,
                                'session' => session_create_id($id)]);
                return Helper::responseIUData($login);
            } else
                return Helper::responseCatchData(415,"Akun tidak ditemukan", 0);
        }catch (\Exception $m){
            return $m;
        }
    }

    static function logout($id){

        try {
            $login = Client::find($id);
            if ($login) {
                $login->update(['idhash' => null,
                                'lastactivity' => time(),
                                'session' => null]);
                return Helper::responseIUData($login);
            } else
                return Helper::responseCatchData(415,"Akun tidak ditemukan", 0);
        }catch (\Exception $m){
            return $m;
        }

    }

    static function checkUser($email){

        try{
            $client = Client::where('email', $email)->count();
            return Helper::responseIUData($client);
        }catch (\Exception $m){
            return $m;
        }
    }



}

<?php

namespace App\Http\Controllers\v1;

use App\Models\v1\Operator;
use App\Models\v1\Ticket;
use App\Rules\OperatorEditFormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\Helper;
use Illuminate\Support\Facades\cache;
use App\Models\v1\Department;
use App\Rules\OperatorFormRequest;
use Throwable;

class OperatorController extends Controller
{
    public function __construct(){}

    public function addOperator(OperatorFormRequest $request){
        try{
            $result = Operator::insert($request);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function editOperator(OperatorEditFormRequest $request){
        try{
            $result = Operator::edit($request);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function sessionOperator($id){
        try{
            $result = Operator::sessionLogin($id);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function logout($id){
        try{
            $result = Operator::logout($id);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function destroy($id){
        try{
            $result = Operator::destroy($id);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }
}

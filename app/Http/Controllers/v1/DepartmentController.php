<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\Helper;
use Illuminate\Support\Facades\cache;
use App\Models\v1\Department;
use App\Rules\TicketFormRequest;
use Throwable;

class DepartmentController extends Controller
{
    public function __construct(){}

    public function getDepartment(){
        try{
            $result = Department::retrieve();
            return $result;
        }catch (\Exception $m){
            return Helper::responseCatchData($m->getCode(), $m->getMessage(), $m->getTraceAsString());
        }
    }

}

<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\Helper;
use Illuminate\Support\Facades\cache;
use App\Models\v1\TicketAnswer;
use App\Rules\TicketAnswerFormRequest;
use Throwable;

class TicketingAnswerController extends Controller
{
    public function __construct(){}

    public function addTicketAnswer(TicketAnswerFormRequest $request){

        try{
            $result = TicketAnswer::insert($request);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }
}

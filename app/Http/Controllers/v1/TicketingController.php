<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\Helper;
use Illuminate\Support\Facades\cache;
use App\Models\v1\Ticket;
use App\Rules\TicketFormRequest;
use Throwable;

class TicketingController extends Controller
{
    public function __construct(){}

    public function getTicket(Request $request){

        try{
            $result = Ticket::retrieve($request);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function addTicket(TicketFormRequest $request){

        try{
            $result = Ticket::insert($request);
            return $result;
        }catch (Exception $m){
            return $m;
        }
    }

    public function getTicketAnswer($id){

        try{
            $result = Ticket::retrieveById($id);
            return $result;
        }catch (Exception $m){
            return $m;
        }
    }

    public function updateTicketAttach($id, $attach){

        try{
            $result = Ticket::updateTicketAttach($id, $attach);
            return $result;
        }catch (Exception $m){
            return $m;
        }
    }

    public function updateTicketStatus($id, $status){

        try{
            $result = Ticket::updateTicketStatus($id, $status);
            return $result;
        }catch (Exception $m){
            return $m;
        }
    }

    public function convertMilisecond(Request $request){

        try{
            $result = Ticket::convertMiliToDate($request->microtime);
            return $result;
        }catch (Exception $m){
            return $m;
        }
    }
}

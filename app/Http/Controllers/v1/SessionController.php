<?php

namespace App\Http\Controllers\v1;

use App\Models\v1\Session;
use App\Models\v1\Ticket;
use App\Rules\CloseChatFromRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\Helper;
use Illuminate\Support\Facades\cache;
use App\Models\v1\Department;
use App\Rules\ClientFormRequest;
use Throwable;

class SessionController extends Controller
{
    public function __construct(){}

    public function closeChat(CloseChatFromRequest $request){
        try{
            $email = $request->email;

            $result = Session::closeChat($email);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }
}

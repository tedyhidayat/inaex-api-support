<?php

namespace App\Http\Controllers\v1;

use App\Models\v1\Client;
use App\Models\v1\Ticket;
use App\Rules\ClientCheckEmailRequest;
use App\Rules\ClientEditFormReuest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\Helper;
use Illuminate\Support\Facades\cache;
use App\Models\v1\Department;
use App\Rules\ClientFormRequest;
use Throwable;

class ClientControlller extends Controller
{
    public function __construct(){}

    public function addClient(ClientFormRequest $request){
        try{
            $result = Client::insert($request);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function editClient(ClientEditFormReuest $request){
        try{
            $result = Client::edit($request);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function loginClient($id){
        try{
            $result = Client::login($id);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function logoutClient($id){
        try{
            $result = Client::logout($id);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }

    public function checkClient(ClientCheckEmailRequest $request){

        try{
            $result = Client::checkUser($request->email);
            return $result;
        }catch (\Exception $m){
            return $m;
        }
    }
}

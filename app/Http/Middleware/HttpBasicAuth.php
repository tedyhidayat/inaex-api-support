<?php

namespace App\Http\Middleware;

use Closure;

class HttpBasicAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->getUser() != 'inaexport' || $request->getPassword() != 'admin123') {
            $headers = array('WWW-Authenticate' => 'Basic');

            return response()->json([
                'code' => 401,
                'message' => 'Unauthorized',
                'headers' => $headers
            ]);
        }

        return $next($request);
    }

}

<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'API Helpdesk is up';
});



$router->group(['middleware' => 'BasicAuth'], function () use ($router) {
    $router->group(['prefix' => 'api/v1'], function () use ($router) {

        //Clear cache
        $router->get('/clear', function () use ($router) {
            return Cache::flush();
        });

        $router->get('/cek_ssh', ['uses' => 'v1\TicketingController@checkSsh']);

        //Ticket
        // $router->get('/ticket', ['uses' => 'v1\TicketingController@getTicket']);
        $router->get('/ticket', ['uses' => 'v1\TicketingController@getTicket']);
        $router->post('/ticket', ['uses' => 'v1\TicketingController@addTicket']);
        $router->post('/convertmili', ['uses' => 'v1\TicketingController@convertMilisecond']);
        $router->get('/ticket/update-attach/{id}/{attach}', ['uses' => 'v1\TicketingController@updateTicketAttach']);
        $router->get('/ticket/update-status/{id}/{status}', ['uses' => 'v1\TicketingController@updateTicketStatus']);

        //Department
        $router->get('/department', ['uses' => 'v1\DepartmentController@getDepartment']);

        //TicketAnswer
        $router->post('ticketanswer',['uses' => 'v1\TicketingAnswerController@addTicketAnswer']);
        $router->get('ticketanswer/{id}', ['uses' => 'v1\TicketingController@getTicketAnswer']);

        //Client
        $router->post('client',['uses' => 'v1\ClientControlller@addClient']);
        $router->get('client/login/{id}',['uses' => 'v1\ClientControlller@loginClient']);
        $router->get('client/logout/{id}',['uses' => 'v1\ClientControlller@logoutClient']);
        $router->post('client/check',['uses' => 'v1\ClientControlller@checkClient']);
        $router->post('client/edit',['uses' => 'v1\ClientControlller@editClient']);

        //Operator
        $router->post('operator',['uses' => 'v1\OperatorController@addOperator']);
        $router->post('operator/edit',['uses' => 'v1\OperatorController@editOperator']);
        $router->get('operator/session/{id}',['uses' => 'v1\OperatorController@sessionOperator']);
        $router->get('operator/logout/{id}',['uses' => 'v1\OperatorController@logout']);
        $router->get('operator/delete/{id}',['uses' => 'v1\OperatorController@destroy']);

        //Session
        $router->get('closechat', ['uses' => 'v1\SessionController@closeChat']);

    });
});
